# Barcode Echo

This program reads a barcode from a connected barcode scanner, formats it as a [ZPL](https://en.wikipedia.org/wiki/Zebra_(programming_language)) file using a template, and then sends that data to a Zebra printer over TCP. The chain of events is something like this:

1. Connect to USB serial port exposed by the barcode scanner (configured in [config.ini](config.ini))
1. Wait for a barcode to be scanned.
1. When the barcode is scanned:
   1. Use it to render a template to form what should be a valid ZPL file
   1. Send that ZPL file to the printer over TCP
   1. Label appears!?
1. Return to step 2

This code was intended to run on Windows.

**Note:** There is a basic batch file version of this script available in the [batch_file](batch_file/) folder.

## Developing this code

Libraries & Tools:

* [pipenv](https://docs.pipenv.org/) was used to set up and maintain the working environment. Run `pipenv install` to set up the environment as defined in `Pipfile` and `pipenv shell` to launch the virtual environment.
* [pyinstaller](http://www.pyinstaller.org/) was used to build standalone executables for Windows. 
* [jinja2](http://jinja.pocoo.org/) was used as the templating engine for the printer job files.

## Building the executable

This can be done by running:

```sh
pyinstaller -F main.py
```

...from the root directory of the project while within the `pipenv` environment. This creates a file called `main.exe` in a folder called `dist`.

Copy this executable to the root directory of the project before running it because it expects `config.ini` to be in the same folder.

## About the barcode scanning

This software was tested with a [Honeywell Xenon 1900 ](https://www.honeywellaidc.com/products/barcode-scanners/general-duty/xenon-1900g-1902g) barcode scanner.

### Device default configuration

It's important to note that the default configuration for this scanner puts it in USB HID mode, where it emulates a USB keyboard; using this would be error-prone as the risk would exist that data from the barcode scanner could get mixed up with data from either an actual keyboard or potentially from the system's clipboard (for example, an accidental *paste* operation).

This scanner can be reconfigured by scanning special barcodes which are defined in the [user manual](files/NG2D-QS.pdf).

### Configuration as tested

In order to improve the reliabiliy of this solution, two key changes to the configuration were made:

1. Configure it to act as a USB serial port by scanning the `USB Serial` barcode. For Windows, this requires specific USB serial drivers available from Honeywell (see "Useful Links" below).

   ![USB Serial](images/usb_serial.png)

1. Configure the scanner to send a carriage return (CR) character after each successful scan; this is done by scanning the `Add CR Suffix` barcode.

   ![Add CR Suffix](images/add_cr_suffix.png)

Scan the `Save` barcode to make these changes semi-permanent:

![Save](images/save.png)

### Optional: Continuous Scanning

Optionally, the barcode scanner can also be set to constantly scan for barcodes rather than depending on someone pulling the trigger by scanning `Streaming Presentation Mode` barcode.

![Streaming Presentation Mode](images/streaming_presentation.png)

## Other Useful Links

* Honeywell Xenon 1900 manual: https://www.honeywellaidc.com/en/-/media/en/files-public/technical-publications/barcode-scanners/Xenon/NG2D-QS.pdf
* Zebra ZPL II Programming Guide: https://www.zebra.com/content/dam/zebra/manuals/en-us/software/zpl-zbi2-pm-en.pdf
* Zebra Data Matrix size calculations: https://support.zebra.com/cpws/docs/general/bxcapacity.htm
* Downloads page for Zebra 1900 (includes serial drivers): https://aidc.honeywell.com/en-US/Pages/Product.aspx?category=hand-held-barcode-scanner&cat=HSM&pid=1900
