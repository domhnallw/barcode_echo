@ECHO OFF

REM Set name of temporary ZPL label file here
SET label_temp_file=label.zpl

REM Set the name of the Windows printer to send the raw ZPL data to
REM Not used when printing via FTP
SET printer_name=ZDesigner GK420d (EPL)

REM Start of main processing loop. Press Ctrl+C to terminate.
:start

REM Delete the temporary file for the label if it exists.
IF EXIST %label_temp_file% DEL %label_temp_file%

REM Read the barcode from standard input
SET /P barcode=Scan barcode: 

REM If the scanned barcode is empty, then wait for another
IF [%barcode%] == [] GOTO start

REM Write barcode ZPL to file
REM ZPL commands use a caret (^) character - this needs to be escaped for ECHO.
REM Start barcode
ECHO ^^XA > %label_temp_file%
REM Position the cursor before printing the barcode
ECHO ^^FO70,100 >> %label_temp_file%
REM Print the actual barcode
ECHO ^^BXN,10,200,, >> %label_temp_file%
ECHO ^^FD%barcode%^^FS >> %label_temp_file%
REM Position the cursor before printing the barcode's contents as a text field
ECHO ^^FO70,55 >> %label_temp_file%
REM Print the barcode contents as a text field.
ECHO ^^CF0,36^^FD%barcode%^^FS >> %label_temp_file%
REM End barcode
ECHO ^^XZ >> %label_temp_file%

REM There are two ways we can get the label to the printer:
REM 1. Over a network via FTP
REM 2. Directly to a local printer (e.g. USB) as a raw blob of data
REM Only one of these should be used at a time. Uncomment one of these options
REM to print the label using this method.

REM Option 1: FTP the label to the printer. The script tells the FTP client
REM which commands to execute.
REM Uncomment the next line to use FTP to send the ZPL.
FTP -i -v -s:ftp_script.txt

REM Option 2: Send the label job to a local printer "raw"
REM Uses RawPrint.exe from http://www.columbia.edu/~em36/windowsrawprint.html
REM Uncomment the next line to send the data "raw" to a local printer
REM RawPrint "%printer_name%" %label_temp_file%

REM Start the scanning loop again (terminate on Ctrl+C)
GOTO start

@ECHO ON