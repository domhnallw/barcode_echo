@ECHO OFF

REM Set name of temporary ZPL label file here
SET label_temp_file=label.zpl

REM Read the barcode from standard input
SET barcode="12345678"

REM Loop from 5 point to 15 point text and generate a label at each size;
REM Send each label to the printer.
FOR /L %%S in (5,1,15) DO (
    REM Delete the temporary file for the label if it exists.
    IF EXIST %label_temp_file% DEL %label_temp_file%

    REM Start barcode
    ECHO ^^XA > %label_temp_file%
    REM Position the cursor before printing the barcode
    ECHO ^^FO30,50 >> %label_temp_file%
    REM Print the actual barcode
    ECHO ^^BXN,8,200,, >> %label_temp_file%
    ECHO ^^FD%barcode%^^FS >> %label_temp_file%
    REM Position the cursor and output the barcode's contents as a text field
    ECHO ^^FO30,30 >> %label_temp_file%
    REM Print the barcode contents as a text field.
    ECHO ^^CFA,%%S^^FDFont Size %%S^^FS >> %label_temp_file%

    REM FTP the label to the printer. The script tells the FTP client
    REM which commands to execute.
    REM See https://support.zebra.com/cpws/docs/znet2/ps_firm/znt2_ftp.htm
    REM Uncomment the next line to use FTP to send the ZPL. Change the IP address
    REM if needed.
    FTP -i -v -s:ftp_script.txt

    REM Send the data directly to the printer via raw sockets
    REM Uses ncat from https://nmap.org/ncat/
    REM Standard port for this is 9100; some Zebra printers use 6101, see
    REM https://www.zebra.com/us/en/support-downloads/knowledge-articles/ait/default-port-number-for-zebra-mobile-printers-is-6101.html
    REM SET raw_data_port=9100
    REM ncat --send-only %printer_address% %raw_data_port% < %label_temp_file%
)

@ECHO ON