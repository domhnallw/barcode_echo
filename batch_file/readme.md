# What's this

This folder contains a minimal implementation of the required barcode scanning using a small dos batch (`.bat`) file.

Unlike the Python version, this requires that the barcode scanner be left in Keyboard/HID mode so that the barcodes are "typed" into the console at scan time.

**Note:** The barcode scanner _must_ be configured as follows:

1. Keyboard/HID Mode:

    ![Keyboard HID Mode](../images/usb_pc_keyboard.png)

1. Send CR after scan:

   ![Add CR Suffix](../images/add_cr_suffix.png)

1. Save settings:

   ![Save](../images/save.png)

## What it does

From once you run the batch script (`barcode.bat`), it will keep requesting a barcode be scanned until a non-empty one is entered (and the `Enter` key is pressed)

Once it gets the barcode, it prints it, wrapped in some simple ZPL commands, into a text file (`label.zpl`). This file is then sent to the printer via FTP, which then prints the label.

## How to use it

Copy the following files to a folder of your choice:

* `barcode.bat`
* `ftp_script.txt`
* `RawPrint.exe`
* `ncat.exe`

Edit `barcode.bat` in your editor of choice (Notepad is fine) and uncomment
(i.e. remove the `REM` comment marker from the beginning of the relevant
line(s)) from the transfer mechanism you want to use, either:

1. **FTP**: Send over the network to a networked printer.
1. **Raw Sockets**: Send data to a network printer directly using raw sockets
1. **Windows Print Driver**: Send raw ZPL commands to a local printer

### Configuring FTP Connectivity

If using FTP, there are three configuration settings that need to be specified;
these are set in the first three lines of `ftp_script.bat`:

```
!REM Connect to the FTP Server
open 10.165.74.143
!REM Enter the username
zebra
!REM Enter the password
admin
```

**Note:** The lines starting with `!REM` are comments and are ignored by the
FTP client.

1. The address of the FTP server offered by the printer.

   This is specified on the first line (the `open` command) in `ftp_script.txt`
1. The username to use when logging in to the FTP server.

   This is the second line of `ftp_script.txt` (`zebra` in the above)
1. The password to use when logging in to the FTP server.

   This is the second line of `ftp_script.txt` (`admin` in the above)

### Setting up the Raw printer support

This approach uses a third-party utility called [RawPrint.exe](http://www.columbia.edu/~em36/windowsrawprint.html)
but this is effectively only a thin layer over some basic GDI instructions to relay raw data to the printing subsystem.

There is only one setting required here - the name of the printer to send the data to.

This is done by setting the `printer_name` variable in `barcode.bat`:

```batch
REM Set the name of the Windows printer to send the raw ZPL data to
SET printer_name=My Printer Name
```

You can get the name of your printer from Windows. Alternatively, either of the following commands will get you a list of printers installed:

```batch
REM "DOS"/Command Prompt
wmic printer get name /value
```

```powershell
# Powershell
Get-WmiObject -Class Win32_printer | ft name, systemName, shareNam
```

### Using Raw Sockets

This approach sends data over raw sockets to the printer using
[ncat](https://nmap.org/ncat/). It requires two configuration options:

1. The address of the printer (set using the `printer_address`) variable:

   ```batch
   REM Set the address of the printer (either IP address or FQDN)
   SET printer_address=10.165.74.143
   ```

1. The TCP port on the printer that accepts raw data. The default port on most
   printers (and most recent Zebra printers is 9100, some older Zebras default
   to 6101).

   This is set using the `raw_data_port` variable; it is set to the default
   port of 9100 so only change it if you need to:

   ```batch
   SET raw_data_port=9100
   ncat --send-only %printer_address% %raw_data_port% < %label_temp_file%
   ```

## Running the program

From this directory, simply type:

```sh
C:\> barcode
```

At this point, the program will display:

```sh
Scan barcode:
```

Scan the barcode with your reader, and the script will send the ZPL data to the
printer using whichever transfer mechanism you selected above.

To exit, press `<Ctrl>+<C>`.

## sizetest.bat? What's that ?

It's basically a quick-and-dirty test script that writes the same barcode data
(12345678) and a text label for each font height in the range 5 to 15
(inclusive) to see which label size is best suited.