import ConfigParser
import io
import serial
import socket
from jinja2 import Template

BARCODE_READER = 'Barcode Scanner'
PRINTER = 'Printer'
TEMPLATE = 'Template'

def configure(filename):
    """
    Load configuration data from file and parse into a dictionary.
    """
    config = ConfigParser.ConfigParser()
    config.read(filename)

    port = config.get(BARCODE_READER, "port")
    speed, data_bits, parity, stop_bits = config.get(BARCODE_READER, "connection").split(",")

    printer_config = {}
    for parameter in ['address', 'connect_using', 'ftp_username', 'ftp_password', 'port']:
        if config.has_option(PRINTER, parameter):
            param_value = config.get(PRINTER, parameter)
            printer_config[parameter] = param_value

    template_config = {}
    for parameter in ['path']:
        param_value = config.get(TEMPLATE, parameter)
        template_config[parameter] = param_value

    return {
        'scanner': {
            'port': port,
            'connection': {
                'speed': int(speed),
                'data_bits': int(data_bits),
                'parity': parity.upper(),
                'stop_bits': int(stop_bits)
            }
        },
        'printer': printer_config,
        'template': template_config
    }

def initialize_reader(configuration):
    """
    Configure and open the serial connection to the barcode scanner.
    """
    reader = serial.Serial()
    connection = configuration['scanner']['connection']
    reader.port = configuration['scanner']['port']
    reader.baudrate = connection['speed']
    reader.bytesize = connection['data_bits']
    reader.parity = connection['parity']
    reader.stopbits = connection['stop_bits']

    reader.open()

    if reader.is_open:
        # The barcode scanner is configured to send a carriage return (\r)
        # at the end of each barcode it scans. Unfortunately, 
        # serial.readline() expects only a newline (\n) so this io wrapper
        # allows us to stop on 
        # 
        serial_io = io.TextIOWrapper(io.BufferedRWPair(reader, reader, 1),
                                     newline = '\r',
                                     line_buffering = True)
        return serial_io, reader
    else:
        # Something else is using that serial port.
        raise RuntimeException("Can't open the serial port")

def initialize_template(configuration):
    """
    Load the template from file
    """
    template_path = configuration['template']['path']

    with open(template_path, 'r') as template_file:
        template_data = ''.join(template_file.readlines())
    
    return Template(template_data)

def send(configuration, label_data):
    """
    Send label data to printer over a direct TCP connection
    """
    raw_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    raw_socket.connect((configuration['printer']['address'], int(configuration['printer']['port'])))
    raw_socket.send(label_data)
    raw_socket.close()

def run(configuration):
    pass

if __name__ == "__main__":
    configuration = configure("config.ini")

    buffered_reader, reader = initialize_reader(configuration)

    print "Reader running"

    template = initialize_template(configuration)

    while True:
        barcode = buffered_reader.readline()
        print "Read barcode", barcode
        label = template.render(barcode = barcode) 
        send(configuration, label)

    # close the serial port.
    reader.close()

    #run(configuration)